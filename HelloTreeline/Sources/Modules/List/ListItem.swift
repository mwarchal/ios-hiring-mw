//
//  ListItem.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

struct ListItem {
    let id: String
    let title: String
}
